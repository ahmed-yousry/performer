<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubmitPapersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('submit_papers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('paper_link');
            $table->string('user_id');
            $table->integer('Track_Selction_id');
            $table->integer('status');

             $table->integer('metadata_id')->nullable();




            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('submit_papers');
    }
}
