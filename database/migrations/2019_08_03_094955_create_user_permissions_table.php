<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserPermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {




         Schema::create('user_permissions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('for');
            $table->timestamps();
        });

        Schema::create('u_roles_user_permissions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('u_roles_id');
            $table->integer('user_permissions_id');

            $table->timestamps();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_permissions');
    }
}
