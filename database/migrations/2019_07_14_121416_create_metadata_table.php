<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMetadataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('metadata', function (Blueprint $table) {
            $table->increments('id');
            $table->string('Title');
            $table->text('Abstract');
            $table->string('Academic_discipline');
            $table->string('Subject_classification');
            $table->string('Keywords');
            $table->string('Language');
             $table->string('URL')->nullable();;
              $table->text('Affilation');
               $table->string('Country');

      


            $table->integer('papers_id');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('metadata');
    }
}
