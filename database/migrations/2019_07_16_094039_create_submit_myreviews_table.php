<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubmitMyreviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('submit_myreviews', function (Blueprint $table) {
            $table->increments('id');
            $table->string('Reviewer_expertise');
            $table->string('Originality');
            $table->string('Quality');
            $table->string('English_Quality');
            $table->text('Detailed_comments');
            $table->text('Comments_to_Proceedings');
            $table->string('Overall_evaluation');            
    
            $table->integer('paper_replies_id');
             $table->integer('user_id');
             $table->integer('paper_id');
          
    

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('submit_myreviews');
    }
}
