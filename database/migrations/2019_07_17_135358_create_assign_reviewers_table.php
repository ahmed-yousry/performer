<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssignReviewersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assign_reviewers', function (Blueprint $table) {
            $table->increments('id');

              $table->integer('user_id');
             $table->integer('paper_id');
             $table->string('accept_date')->nullable();;
             $table->string('Due')->nullable();;
             
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     * 
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assign_reviewers');
    }
}
