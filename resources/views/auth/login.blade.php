

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

        <title>Performer Ierek</title>

      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.2/css/all.css" />
        <!-- Style -->
  <link rel="stylesheet" href="{{ asset('assets/website/css/bootstrap.min.css')}}" />

            <link rel="stylesheet" href="{{ asset('assets/website/css/style.css')}}" />
        <style>
            .alert-warning {
                color: #856404;
                background-color: #ef292970 !important;
                color: #fff;
                font-size: .9em !important;
                padding: 5px;
                border: none !important;
            }
            .alert-warning a{color: #fff; font-weight:bold;}
            .alert-success{
                color: #ffffff !important;
                background-color: #23983f9e !important;
                border: none !important;
                padding: 5px;
            }
            .loginPage .invalid-feedback{
                border-radius: 10px !important;
                /* margin-bottom: 18px; */
                padding: 5px;
                background: #ff5c5c80 !important;
                color: #ffff;
                margin: 5px 0px !important;
                display: block !important;
                padding: 6px;
                font-size: .8em;
            }
            .loginPage .form-group{
                margin: 0;
            }
        </style>
    </head>
    <body>
        <section class="bgApp loginPage" bgpage="bg-app">
            <div class="con-app row m-0 container-fluid ">

         <form method="POST"   class="row form-app col-12 p-0 col-md-7 col-lg-9 mx-auto"  action="{{ route('login') }}" aria-label="{{ __('Login') }}">
                        @csrf

                    <h1 class="col-12 text-center ">
                        <div class="brand-e">
                                <div class="ierek">
                                            IEREK
                                </div>
                                <div class="ierek-s">
                                    S
                                </div>
                        </div>
                    </h1>
                    <p class="col-12 lead text-center">Conference Paper Submission and Evaluation System</p>
<!--                     <p class="col-12 lead text-center">2nd Euro-Mediterranean Conference for Environmental Integration
                            EMCEI-2, 10-13 October 2019, Sousse, Tunisia</p> -->
                    <div class="col-12 p-0 row m-0 div-golfo" author="loginA">
                            <div class="col-12 col-md-6 mt-3">
                                    <h5 class="text-center head-author">Already have an account ?</h5>
                                    <div class="line-gr">
                                        <div class="w-100 mb-3">
                                            <div class="form-group">
                                                <span class="ts-icon"><i class="fas fa-at"></i></span>
                                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                                            </div>

                                            @if ($errors->has('email'))
                                                <span class="invalid-feedback d-block" role="alert">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif




                                        </div>
                                        <div class="w-100 mb-3">
                                        <div class="form-group">
                                            <span class="ts-icon"><i class="fas fa-unlock-alt"></i></span>

                      <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif


                                        </div>
                                        </div>

                                    </div>


                                    @if (session('status'))
                   <div class="alert alert-success">
                       {{ session('status') }}
                   </div>
               @endif

                                    @if (session('warning'))
                   <div class="alert alert-warning">
                       {{ session('warning') }}
                       <br>
                       <a href="user/resend/verify/{{ session('user_id') }}">{{ session('link') }}</a>





                   </div>
               @endif



                                          <button type="submit" class="btn w-100">
                                    {{ __('Login') }}
                                </button>

                                    <div class="w-100 text-center mt-3 foot-form">
                                        <div class="form-check">
  <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('Remember Me') }}

                                        </div>

                                 <a href="{{ route('password.request') }}">
                                    {{ __('Forgot Your Password?') }}
                                </a>

                                    </div>
                            </div>
                            <div class="col-12 col-md-6 mt-3">
                                <h5 class="text-center head-author">Submit a new paper</h5>
                                <p class="h6 text-center m-0 pb-2" author="p">In order for you to be able to submit a paper, you must first create an author account. The author account will allow you to submit multiple papers. This will allow for having a single account for all your papers.</p>
                                <a href="{{url('register')}}" class="btn w-100"><i class="fas fa-user-circle"></i> Create Account</a>
                            </div>
                    </div>

                </form>
            </div>
        </section>
        <!-- Script -->
       <script src="{{ asset('assets/website/js/jquery-3.4.1.min.js')}}"></script>
        <script src="{{ asset('assets/website/js/popper.min.js')}}"></script>

        <script src="{{ asset('assets/website/js/bootstrap.min.js')}}"></script>



        <script src="{{ asset('assets/website/js/app.js')}}"></script>
    </body>
</html>
