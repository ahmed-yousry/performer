@extends('admin.layouts.master')
@section('title')
    الادمن | الحضور
@endsection
@section('page-header')
    <section class="content-header">
        <h1>
            Home Page
            <small></small>
        </h1>

    </section>
@endsection

@section('content')

    <section class="content">

        <div class="row">
            <div class="col-md-12">
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">
                            الحضور </h3>
                        <div class="box-tools">
                            <div class="input-group input-group-sm" style="width: 150px;">
                                <input type="text" name="table_search" class="form-control pull-right"
                                       placeholder="Search">

                                <div class="input-group-btn">
                                    <button type="submit" class="btn btn-default">
                                        <i class="fa fa-search"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <tr>
                                <th>اسم الطالب</th>
                                <th>مجموعة</th>
                                <th>حضور</th>
                                <th>تاريخ التسجيل</th>
                            </tr>
                                <tr>
                                    <td>ِAhmed Ali</td>
                                    <td>A</td>
                                    <td>حضور</td>
                                    <td>01/09/2018</td>

                                </tr>
                                <tr>
                                    <td>Yasmin Farouk</td>
                                    <td>B</td>
                                    <td>غياب</td>
                                    <td>15/09/2018</td>

                                </tr>
                                <tr>
                                    <td>Hader Gamal</td>
                                    <td>C</td>
                                    <td>حضور</td>
                                    <td>15/08/2018</td>

                                </tr>
                        </table>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer clearfix">
                        <ul class="pagination pagination-sm no-margin pull-right">
                            <li><a href="#">&laquo;</a></li>
                            <li><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">&raquo;</a></li>
                        </ul>
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>

        <br>

    </section>

@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/bower_components/lightbox2-master/lightbox.css')}}">
@endsection

@section('js')

    <script src="{{ asset('assets/bower_components/lightbox2-master/lightbox.js')}}"></script>

@endsection
