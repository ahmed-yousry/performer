@extends('admin.layouts.master')
@section('title')
    تسجيل حضور الموظف
@endsection
@section('page-header')
    <section class="content-header">
        <h1>
            تسجيل حضور الموظف
            <small></small>
        </h1>

    </section>
@endsection

@section('content')

    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form class="form-horizontal" method="post" action="{{url('/admin/attendance-employee')}}" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="box-body">
                            <div class="form-group">
                                <label for="category" class="col-sm-4 control-label">الموظف</label>
                                <div class="col-sm-4">
                                    <select name="employee_id" id="subject_id" class="select2 form-control ">
                                        @foreach($employees as $employee)
                                                <option value="{{$employee->id}}">{{$employee->fname}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                                <div class="col-sm-4 col-sm-offset-5" >
                                    <div class="form-group">
                                        <label>
                                            <input type="radio" name="state" value="attend" class="minimal" checked> حضور
                                        </label>
                                        <label>
                                            <input type="radio" name="state" value="go" class="minimal">انصراف
                                        </label>
                                    </div>
                                </div>


                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-info center-block">تسجيل<i class="fa fa-save" style="margin-left: 5px"></i></button>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                </div>
                <!-- /.box -->
                <!-- general form elements disabled -->

                <!-- /.box -->
            </div>
            <!--/.col (right) -->
        </div>
        <!-- /.row -->
    </section>

@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/bower_components/jQuery-Tags-Input-master/dist/jquery.tagsinput.min.css')}}">
    <style>
        div.tagsinput span.tag {
            border: 1px solid #66c0e0;
            -moz-border-radius: 2px;
            -webkit-border-radius: 2px;
            display: block;
            float: left;
            padding: 5px;
            text-decoration: none;
            background: #66c0e0;
            color: #ffffff;
            margin-right: 5px;
            margin-bottom: 5px;
            font-family: helvetica;
            font-size: 14px;
            border-radius: 10px;
        }
        div.tagsinput span.tag a{
            color: white;
        }
    </style>
    <link rel="stylesheet" href="{{ asset('assets/bower_components/select2/dist/css/select2.min.css')}}">
    <style>
            .select2 {
                width:100%!important;
                }
                .select2-selection { overflow: hidden; }
.select2-selection__rendered { white-space: normal; word-break: break-all; }
    </style>
    <style>
        .select2-container--default .select2-selection--multiple .select2-selection__choice{
            background-color: #0d6aad;
            border: none;
        }
    </style>
@endsection

@section('js')
    <!-- CK Editor -->
    <script src="{{ asset('assets/bower_components/ckeditor/ckeditor.js')}}"></script>


    <script src="{{ asset('assets/bower_components/jQuery-Tags-Input-master/dist/jquery.tagsinput.min.js')}}"></script>
    <script>
        $('#meta_keywords').tagsInput({
            // 'height':'34px',
            'width':'315px',
            'defaultText':'',
        });
    </script>


    <script src="{{ asset('assets/bower_components/select2/dist/js/select2.min.js')}}"></script>

    <script>
        $('.select2').select2()
    </script>

@endsection


