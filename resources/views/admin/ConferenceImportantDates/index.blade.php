@extends('admin.layouts.master')
@section('title')
Conference Important Dates
@endsection
@section('page-header')
    <section class="content-header">
        <h1>
Conference Important Dates     <small></small></h1>

    </section>
@endsection

@section('content')


    <section class="content">

        <div class="row">
        <div class="col-md-12">


                <!-- <a href="{{url('/admin/class/create')}}" class="btn btn-primary pull-right margin-bottom">
                    <i class="fa fa-plus"></i>
Add Conference Important Dates                </a> -->


        </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">
                        </h3>
                        {{--<div class="box-tools">--}}
                            {{--<div class="input-group input-group-sm" style="width: 150px;">--}}
                                {{--<input type="text" name="table_search" class="form-control pull-right"--}}
                                       {{--placeholder="Search">--}}

                                {{--<div class="input-group-btn">--}}
                                    {{--<button type="submit" class="btn btn-default">--}}
                                        {{--<i class="fa fa-search"></i></button>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    </div>

                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <tr>
                                <th>   start Date      </th>
                                <th> End Date  </th>
                                <th> Action  </th>
                            </tr>
                            @foreach($ConferenceImportantDates as $ConferenceImportantDate)
                                <tr>
                                    <td>{{$ConferenceImportantDate->start_date}}</td>
                                    <td>{{$ConferenceImportantDate->end_date}}</td>



                                    <td>

                                            <a href="{{url('/admin/users/ConferenceImportantDates/show/edit/'.$ConferenceImportantDate->id)}}" class="btn btn-info btn-circle"><i class="fa fa-edit"></i></a>


                                                <!-- <a href="{{url('admin/users/ConferenceImportantDates/'.$ConferenceImportantDate->id.'/delete')}}" class="btn btn-danger btn-circle"><i class="fa fa-trash-o"></i></a> -->

                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer clearfix">

                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>


        <br>

    </section>

@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/bower_components/lightbox2-master/lightbox.css')}}">
@endsection

@section('js')

    <script src="{{ asset('assets/bower_components/lightbox2-master/lightbox.js')}}"></script>

@endsection
