@extends('admin.layouts.master')
@section('title')
Add Conference Important Dates
@endsection
@section('page-header')
    <section class="content-header">
        <h1>
Add Conference Important Dates
            <small></small>
        </h1>

    </section>
@endsection

@section('content')

    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form class="form-horizontal" method="post" action="{{url('/admin/users/ConferenceImportantDates/show/edit/'.$ConferenceImportantDates->id)}}" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <!-- <input name="_method" type="hidden" value="PATCH"> -->

                        <div class="box-body">

                            <div class="form-group">
                                <label for="title" class="col-sm-4 control-label">  Start Dates    </label>
                                <div class="col-sm-4 {{ $errors->has('start_date') ? ' has-error' : '' }}">
                                    <input type="date" name="start_date" value="{{$ConferenceImportantDates->start_date}}" class="form-control" id="ConferenceImportantDates" placeholder="start date" value="{{ old('start_date') }}" required autofocus>
                                    @if ($errors->has('start_date'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('start_date') }}</strong>
                                        </span>
                                    @endif
                                </div>


                            </div>
                            <div class="form-group">
                                    <label for="education_year_id" class="col-sm-4 control-label"> End date</label>

                                    <div class="col-sm-4">
                                      <input type="date" name="end_date" value="{{$ConferenceImportantDates->end_date}}" class="form-control" id="end_date" placeholder="start end_date" value="{{ old('end_date') }}" required autofocus>

                                    </div>

                                    @if ($errors->has('end_date'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('end_date') }}</strong>
                                        </span>
                                    @endif

                            </div>


                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-info center-block">Save   <i class="fa fa-save" style="margin-left: 5px"></i></button>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                </div>
                <!-- /.box -->
                <!-- general form elements disabled -->

                <!-- /.box -->
            </div>
            <!--/.col (right) -->
        </div>
        <!-- /.row -->
    </section>

@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/bower_components/jQuery-Tags-Input-master/dist/jquery.tagsinput.min.css')}}">

    <link rel="stylesheet" href="{{ asset('assets/bower_components/select2/dist/css/select2.min.css')}}">
    <style>
            .select2 {
                width:100%!important;
                }
                .select2-selection { overflow: hidden; }
.select2-selection__rendered { white-space: normal; word-break: break-all; }
    </style>
    <style>
        .select2-container--default .select2-selection--multiple .select2-selection__choice{
            background-color: #0d6aad;
            border: none;
        }
    </style>
@endsection

@section('js')
    <!-- CK Editor -->
    <script src="{{ asset('assets/bower_components/ckeditor/ckeditor.js')}}"></script>


    <script>
        $(function () {
            // Replace the <textarea id="editor1"> with a CKEditor
            // instance, using default configuration.
            CKEDITOR.replace('editor1')
            CKEDITOR.replace('editor2')
            CKEDITOR.replace('editor3')
            CKEDITOR.replace('editor4')
            CKEDITOR.replace('editor5')
            //bootstrap WYSIHTML5 - text editor
            $('.textarea').wysihtml5()
        })
    </script>
    <script src="{{ asset('assets/bower_components/jQuery-Tags-Input-master/dist/jquery.tagsinput.min.js')}}"></script>
    <script>
        $('#meta_keywords').tagsInput({
            // 'height':'34px',
            'width':'315px',
            'defaultText':'',
        });
    </script>


    <script src="{{ asset('assets/bower_components/select2/dist/js/select2.min.js')}}"></script>

    <script>
        $('.select2').select2()
    </script>

@endsection
