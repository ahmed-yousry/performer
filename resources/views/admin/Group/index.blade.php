@extends('admin.layouts.master')
@section('title')
    المجموعات الدراسية
@endsection
@section('page-header')
    <section class="content-header">
        <h1>
                المجموعات الدراسية
                <small></small></h1>

    </section>
@endsection

@section('content')


    <section class="content" id="app">

        <div class="row">
        <div class="col-md-12">
            @if(\Illuminate\Support\Facades\Auth::guard('admin')->user()->can('group.create'))
                <a href="{{url('/admin/group/create')}}" class="btn btn-primary pull-right margin-bottom">
                    <i class="fa fa-plus"></i>
                    اضافة جروب جديد
                </a>
            @endif

        </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">
                        </h3>
                        <div class="box-tools">
                            <div class="input-group input-group-sm" style="width: 150px;">
                                <input type="text" name="table_search" v-model="search" v-on:keyup.enter="searchData" class="form-control pull-right"
                                       placeholder="Search">

                                <div class="input-group-btn">
                                    <button type="submit" class="btn btn-default" >
                                        <i class="fa fa-search"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <tr>
                                 <th>الاسم</th>
                                <th>موعدالبداية</th>
                                <th>موعد الانتهاء</th>
                                <th>المدرس</th>
                                <th>المادة</th>
                                <th>السنة الدراسية</th>

                                <th>عمليات</th>
                            </tr>
                                <tr v-for="group in groups">
                                    <td>@{{group.name}}</td>
                                    <td>@{{group.from}}</td>
                                    <td>
                                        @{{group.to}}
                                    </td>
                                    <td>
                                        @{{group.teacher.fname}}
                                    </td>

                                    <td>
                                        @{{group.subject.name}}
                                    </td>
                                    <td>
                                        @{{group.classes.name}}
                                    </td>

                                    <td>
                                        @if(\Illuminate\Support\Facades\Auth::guard('admin')->user()->can('group.update'))
                                            <a href="" v-on:click.prevent="edit(group.id)" class="btn btn-info btn-circle"><i class="fa fa-edit"></i></a>
                                        @endif
                                        @if(\Illuminate\Support\Facades\Auth::guard('admin')->user()->can('group.delete'))
                                            <a href="" v-on:click.prevent="deleteGroup(group.id)" class="btn btn-danger btn-circle"><i class="fa fa-trash-o"></i></a>
                                        @endif


                                        {{--@if(\Illuminate\Support\Facades\Auth::guard('admin')->user()->can('group.update'))--}}
                                            {{--<a href="{{url('/admin/group/'.group.id.'/edit')}}" class="btn btn-info btn-circle"><i class="fa fa-edit"></i></a>--}}
                                        {{--@endif--}}
                                        {{--@if(\Illuminate\Support\Facades\Auth::guard('admin')->user()->can('group.delete'))--}}
                                                {{--<a href="{{url('admin/group/'.group.id.'/delete')}}" class="btn btn-danger btn-circle"><i class="fa fa-trash-o"></i></a>--}}
                                        {{--@endif--}}
                                    </td>
                                </tr>
                        </table>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer clearfix">
                        <ul class="pagination pagination-sm no-margin pull-right">
                            <li><a href="#">&laquo;</a></li>
                            <li><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">&raquo;</a></li>
                        </ul>
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>


        <br>

    </section>

@endsection

@section('css')

@endsection

@section('js')
    <script src="https://cdn.jsdelivr.net/npm/vue"></script>
    <script src="https://unpkg.com/vue-swal"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue-select/2.5.1/vue-select.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.15.2/axios.js"></script>
    <script type="text/javascript" src="{{asset('assets/main/group.js')}}"></script>

@endsection
