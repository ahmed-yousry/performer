@extends('admin.layouts.master')
@section('title')
اضافه مدرس جدد@endsection

@section('page-header')
    <section class="content-header">
        <h1>
اضافه مدرس جدد            <small></small>
        </h1>

    </section>
@endsection 

@section('content')

    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">   </h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->


                    <form class="form-horizontal" method="post" action="{{url('/admin/teacher/add')}}">
                        {{csrf_field()}}
                    <div class="box-body">
                    <div class="form-group">

                    <label for="username" class="col-sm-4 control-label">الاسم كامل </label>

                        <div class="col-sm-4 {{ $errors->has('fname') ? ' has-error' : '' }}">
                            <input type="text" name="fname" class="form-control" id="username" placeholder="الاسم كامل" value="{{ old('fname') }}">
                            @if ($errors->has('fname'))
                                <span class="help-block">
                    <strong>{{ $errors->first('fname') }}</strong>
                    </span>
                            @endif
                        </div>

                    </div>
                    
                    <div class="form-group">

                            <label for="phone" class="col-sm-4 control-label"> رقم الموبيل </label>

                            <div class="col-sm-4 {{ $errors->has('phone') ? ' has-error' : '' }}">
                                <input type="text" name="phone" class="form-control" id="phone" placeholder="قم الموبيل" value="{{ old('phone') }}">
                                @if ($errors->has('phone'))
                                    <span class="help-block">
                        <strong>{{ $errors->first('phone') }}</strong>
                        </span>
                                @endif
                            </div>
                    </div>


                         <div class="form-group">

<label for="username" class="col-sm-4 control-label"> البريد  الاكترونى </label>

    <div class="col-sm-4 {{ $errors->has('email') ? ' has-error' : '' }}">
        <input type="email" name="email" class="form-control" id="email" placeholder="البريد  الاكترونى" value="{{ old('email') }}">
        @if ($errors->has('email'))
            <span class="help-block">
<strong>{{ $errors->first('email') }}</strong>
</span>
        @endif
    </div>

</div>



  <div class="form-group">

<label for="password" class="col-sm-4 control-label">  الرقم السرى  </label>

    <div class="col-sm-4 {{ $errors->has('password') ? ' has-error' : '' }}">
        <input type="password" name="password" class="form-control" id="password" placeholder="الرقم  السرى" value="{{ old('password') }}">
        @if ($errors->has('password'))
            <span class="help-block">
<strong>{{ $errors->first('password') }}</strong>
</span>
        @endif
    </div>

</div>



  <div class="form-group">

<label for="cpassword" class="col-sm-4 control-label"> تكرار  الرقم السرى </label>

    <div class="col-sm-4 {{ $errors->has('cpassword') ? ' has-error' : '' }}">
        <input type="password" name="password_confirmation" class="form-control" id="cpassword" placeholder="تكرار الرقم  السرى" value="{{ old('cpassword') }}">
        @if ($errors->has('cpassword'))
            <span class="help-block">
<strong>{{ $errors->first('cpassword') }}</strong>
</span>
        @endif
    </div>

</div>





                            <div class="form-group">
                                <label for="category" class="col-sm-4 control-label">مواد التدريس</label>
                                <div class="col-sm-4 ">
                                    <select name="subject_id[]" id="subject_id" class="select2 select form-control " multiple>
                                        @foreach($subjects as $sub)
                                                <option value="{{$sub->id}}">{{$sub->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                    </div>




                    <div class="box-footer">
                    <button type="submit" class="btn btn-info center-block">save</button>
                    </div>

                    </form>
                </div>

            </div>
        </div>
    </section>

@endsection

@section('css')

    <link rel="stylesheet" href="{{ asset('assets/bower_components/select2/dist/css/select2.min.css')}}">
    <style>
            .select2 {
                width:100%!important;
                }
                .select2-selection { overflow: hidden; }
.select2-selection__rendered { white-space: normal; word-break: break-all; }
    </style>
@endsection

@section('js')

    <script src="{{ asset('assets/bower_components/select2/dist/js/select2.min.js')}}"></script>


    <script>
        $('.select2').select2()
    </script>
@endsection


