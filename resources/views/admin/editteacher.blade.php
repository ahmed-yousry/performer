@extends('admin.layouts.master')
@section('title')
  تعديل مدرس@endsection

@section('page-header')
    <section class="content-header">
        <h1>
  تعديل مدرس            <small></small>
        </h1>

    </section>
@endsection

@section('content')

    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">   </h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->


                    <form class="form-horizontal" method="post" action="{{url('/admin/teacher/show/edit').'/'.$user->id}}">
                        {{csrf_field()}}
                    <div class="box-body">
                    <div class="form-group">

                    <label for="username" class="col-sm-4 control-label">الاسم كامل </label>

                        <div class="col-sm-4 {{ $errors->has('fname') ? ' has-error' : '' }}">
                            <input type="text" name="fname" class="form-control" id="username" value="{{$user->fname}}"  placeholder="الاسم كامل">
                            @if ($errors->has('fname'))
                                <span class="help-block">
                    <strong>{{ $errors->first('fname') }}</strong>
                    </span>
                            @endif
                        </div>


                    </div>
                    <div class="form-group">
                            <label for="phone" class="col-sm-4 control-label"> رقم الموبيل </label>

                            <div class="col-sm-4 {{ $errors->has('phone') ? ' has-error' : '' }}">
                                <input type="text" name="phone" class="form-control" id="phone" placeholder="قم الموبيل"  value="{{$user->phone}}"  >
                                @if ($errors->has('phone'))
                                    <span class="help-block">
                        <strong>{{ $errors->first('phone') }}</strong>
                        </span>
                                @endif
                            </div>
                    </div>
                    
                    
                    <!--  <div class="form-group">-->

                    <!--<label for="lname" class="col-sm-4 control-label">الاسم الثانى </label>-->

                    <!--    <div class="col-sm-4 {{ $errors->has('lname') ? ' has-error' : '' }}">-->
                    <!--        <input type="text" name="lname" class="form-control" id="lname" placeholder="لاسم الثانى" value="{{ old('lname') }}">-->
                    <!--        @if ($errors->has('lname'))-->
                    <!--            <span class="help-block">-->
                    <!--<strong>{{ $errors->first('lname') }}</strong>-->
                    <!--</span>-->
                    <!--        @endif-->
                    <!--    </div>-->

                    <!--</div>-->
                    
                    
                    <!--  <div class="form-group">-->

                    <!--<label for="tname" class="col-sm-4 control-label">الاسم الثالث </label>-->

                    <!--    <div class="col-sm-4 {{ $errors->has('tname') ? ' has-error' : '' }}">-->
                    <!--        <input type="text" name="tname" class="form-control" id="tname" placeholder="لاسم الثالث" value="{{ old('tname') }}">-->
                    <!--        @if ($errors->has('tname'))-->
                    <!--            <span class="help-block">-->
                    <!--<strong>{{ $errors->first('tname') }}</strong>-->
                    <!--</span>-->
                    <!--        @endif-->
                       
                    <!--</div> </div>-->

                    
                            <div class="form-group">
                                <label for="category" class="col-sm-4 control-label">مواد التدريس</label>
                                <div class="col-sm-4 ">
                                    <select name="subject_id[]" id="subject_id" class="select2 form-control " multiple>
                                        @foreach($subjects as $sub)
                                            <option value="{{$sub->id}}"
                                                    @foreach($user->subjects as $calss_sub)
                                                        @if($calss_sub->id == $sub->id)
                                                        selected
                                                        @endif
                                                    @endforeach
                                            >{{$sub->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                    
                    
                    
           
                    
                    
                  
                  



                      



                 



                    </div>




                    <div class="box-footer">
                    <button type="submit" class="btn btn-info center-block">save</button>
                    </div>

                    </form>
                </div>

            </div>
        </div>
    </section>

@endsection

@section('css')

    <link rel="stylesheet" href="{{ asset('assets/bower_components/select2/dist/css/select2.min.css')}}">
    <style>
            .select2 {
                width:100%!important;
                }
                .select2-selection { overflow: hidden; }
.select2-selection__rendered { white-space: normal; word-break: break-all; }
    </style>

    @endsection

@section('js')

    <script src="{{ asset('assets/bower_components/select2/dist/js/select2.min.js')}}"></script>


    <script>
        $('.select2').select2()
    </script>
@endsection


