@extends('admin.layouts.master')
@section('title')
    حضور الطلاب
@endsection
@section('page-header')
    <section class="content-header">
        <h1>
            حضور الطلاب            <small></small>
        </h1>

    </section>
@endsection

@section('content')

    <section class="content" id="app">
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form class="form-horizontal" method="post" action="{{url('/admin/attendance-student')}}" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="box-body">
                            <ul>
                            </ul>
                            <div class="form-group">
                                <label for="edu_id" class="col-sm-1 control-label">المرحلة</label>
                                <div class="col-sm-5">
                                    <select name="edu_id" id="edu_id" v-on:change="getClass" v-model="edu_id" class="form-control ">
                                        <option  v-for="edu in educations" v-bind:value="edu.id"  >@{{ edu.name }}</option>
                                    </select>
                                </div>
                                <label for="class_id" class="col-sm-1 control-label">السنة الدراسية</label>
                                <div class="col-sm-5">
                                    <select name="class_id" id="edu_id" v-on:change="getSubject" v-model="class_id" class="form-control ">
                                        <option  v-for="cl in classes" v-bind:value="cl.id"  >@{{ cl.name }}</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="group_id" class="col-sm-1 control-label">المواد</label>
                                <div class="col-sm-11">
                                    <select name="class_id" id="edu_id" v-model="subject_id" v-on:change="getDate" class="form-control ">
                                        <option  v-for="g in subjects" v-bind:value="g.id"  >@{{g.name}}</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="date" class="col-sm-1 control-label">التاريخ</label>
                                <div class="col-sm-11 ">
                                    <input type="date" name="date" v-model="dated"  v-on:change="getDate" id="date" class="form-control ">
                                </div>
                            </div>
                            <!-- /.form group -->

                            <div class="form-group">
                                {{--<label for="title" class="col-sm-1 control-label">اسم الطالب</label>--}}
                                {{--<div class="col-sm-11 {{ $errors->has('from') ? ' has-error' : '' }}">--}}
                                    {{--<select class="form-control select2"  v-bind:id="select2" style="width: 100% ;" name="select2" v-model="student_id"  v-on:change="test">--}}
                                    {{--<option v-for="student in students" v-bind:value="student.id">@{{ student.fname }}</option>--}}
                                    {{--</select>--}}

                                    {{--<v-select :options="options" label="fname"  v-model="student" v-on:change="test"></v-select>--}}

                                {{--</div>--}}
                            </div>

                            {{--<div class="form-group">--}}
                            {{--<div class='input-group date' id='datetimepicker1'>--}}
                            {{--<input type='text' class="form-control" />--}}
                            {{--<span class="input-group-addon">--}}
                            {{--<span class="glyphicon glyphicon-calendar"></span>--}}
                            {{--</span>--}}
                            {{--</div>--}}
                            {{--</div>--}}



                        </div>
                        {{--<!-- /.box-body -->--}}
                        {{--<div class="box-footer">--}}
                            {{--<button type="submit" v-on:click.prevent="addstudent"  v-bind:disabled="btnDisable" class="btn btn-info center-block">تسجيل حضور<i class="fa fa-save" style="margin-left: 5px"></i></button>--}}
                        {{--</div>--}}
                    </form>
                    <hr>
                    <!-- /.box-header -->


                </div>
                <!-- /.box -->
                <!-- general form elements disabled -->

                <!-- /.box -->
            </div>
            <div class="col-md-6">
                <div class="box box-info">
                    <div class="box-header with-border">
                       الطلاب الحاضرين  (@{{ studentss.length }})
                    </div>
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tr>
                            <th>اسم الطالب</th>
                            <th>اسم المجموعة</th>
                            <th>حضور</th>
                        </tr>
                        <tr v-for="i in studentss">
                            <td>@{{i.studentName}}</td>
                            <td>@{{i.groupName}}</td>

                            <td>
                                <i class="fa fa-check" aria-hidden="true"></i>
                            </td>

                        </tr>
                    </table>
                </div>
                </div>

            </div>
            <div class="col-md-6">
                <div class="box box-info">
                    <div class="box-header with-border">
                        الطلاب الغائبين (@{{ notstudents.length }})
                    </div>
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <tr>
                                <th>اسم الطالب</th>
                                <th>حضور</th>
                            </tr>
                            <tr v-for="i in notstudents">
                                <td>@{{i.fname}}</td>

                                <td>
                                    <i class="fa fa-times" aria-hidden="true"></i>

                                </td>

                            </tr>
                        </table>
                    </div>
                </div>            </div>
            <!--/.col (right) -->
        </div>
        <!-- /.row -->
    </section>

@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/bower_components/select2/dist/css/select2.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}" />



@endsection

@section('js')
    <script src="{{ asset('assets/bower_components/select2/dist/js/select2.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
    <!-- date-range-picker -->
    <script src="https://cdn.jsdelivr.net/npm/vue"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue-select/2.5.1/vue-select.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.15.2/axios.js"></script>

    <script type="text/javascript" src="{{asset('assets/main/attendance_student_index.js')}}"></script>

    <script>
        $('.select2').select2()
        //Date picker
        $('#datepicker').datepicker({
            autoclose: true
        })
    </script>

    {{--<script type="text/javascript">--}}
    {{--$(function () {--}}
    {{--$('#datetimepicker1').datetimepicker();--}}
    {{--});--}}
    {{--</script>--}}



@endsection
