@extends('admin.layouts.master')
@section('title')
حضور الطلاب
@endsection
@section('page-header')
    <section class="content-header">
        <h1>
حضور الطلاب            <small></small>
        </h1>

    </section>
@endsection

@section('content')

    <section class="content" id="app">
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form class="form-horizontal" method="post" action="{{url('/admin/attendance-student')}}" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="box-body">
                            <div class="form-group">
                                <label for="edu_id" class="col-sm-1 control-label">المرحلة</label>
                                <div class="col-sm-5">
                                    <select name="edu_id" id="edu_id" v-on:change="getClass" v-model="edu_id" class="form-control ">
                                            <option  v-for="edu in educations" v-bind:value="edu.id"  >@{{ edu.name }}</option>
                                    </select>
                                </div>

                                <label for="class_id" class="col-sm-1 control-label">السنة الدراسية</label>
                                <div class="col-sm-5">
                                    <select name="class_id" id="edu_id" v-on:change="getGroup" v-model="class_id" class="form-control ">
                                        <option  v-for="cl in classes" v-bind:value="cl.id"  >@{{ cl.name }}</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="group_id" class="col-sm-1 control-label">المجموعة</label>
                                <div class="col-sm-11">
                                    <select name="class_id" id="edu_id" v-model="group_id" class="form-control ">
                                        <option  v-for="g in groups" v-bind:value="g.id"  >@{{ g.from+' - '+g.to+'   |  استاذ : '+ g.teacher_id+' | '+g.subject_id  }}</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="title" class="col-sm-1 control-label">اسم الطالب</label>
                                <div class="col-sm-11 {{ $errors->has('from') ? ' has-error' : '' }}">
                                    {{--<select class="form-control select2"  v-bind:id="select2" style="width: 100% ;" name="select2" v-model="student_id"  v-on:change="test">--}}
                                        {{--<option v-for="student in students" v-bind:value="student.id">@{{ student.fname }}</option>--}}
                                    {{--</select>--}}

                                    <v-select :options="options" label="fname"  dir="rtl"  v-model="student" v-on:change="test"></v-select>

                                </div>
                            </div>

                            {{--<div class="form-group">--}}
                                {{--<div class='input-group date' id='datetimepicker1'>--}}
                                    {{--<input type='text' class="form-control" />--}}
                                    {{--<span class="input-group-addon">--}}
                        {{--<span class="glyphicon glyphicon-calendar"></span>--}}
                    {{--</span>--}}
                                {{--</div>--}}
                            {{--</div>--}}



                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" :disabled="!isComplete"  v-on:click.prevent="addstudent" class="btn btn-info center-block">تسجيل حضور<i class="fa fa-save" style="margin-left: 5px"></i></button>

                        </div>
                        <!-- /.box-footer -->
                    </form>
                </div>
                <!-- /.box -->
                <!-- general form elements disabled -->

                <!-- /.box -->
            </div>
            <!--/.col (right) -->
        </div>
        <!-- /.row -->
    </section>

@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/bower_components/select2/dist/css/select2.min.css')}}">
{{--    <link rel="stylesheet" href="{{asset('assets/bower_components/bootstrap/dist/css/bootstrap.min.css')}}" />--}}
    {{--<link rel="stylesheet" href="{{asset('assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.css')}}" />--}}



@endsection

@section('js')
    <script src="{{ asset('assets/bower_components/select2/dist/js/select2.min.js')}}"></script>
    {{--<script type="text/javascript" src="{{asset('assets/bower_components/dist/jquery/jquery.js')}}"></script>--}}
    {{--<script type="text/javascript" src="{{asset('assets/bower_components/moment/min/moment.min.js')}}"></script>--}}
    {{--<script type="text/javascript" src="{{asset('assets/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>--}}
    {{--<script type="text/javascript" src="{{asset('assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.js')}}"></script>--}}
    <script src="https://cdn.jsdelivr.net/npm/vue"></script>
    <script src="https://unpkg.com/vue-swal"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue-select/2.5.1/vue-select.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.15.2/axios.js"></script>

    <script type="text/javascript" src="{{asset('assets/main/attendance_student.js')}}"></script>

    <script>
        $('.select2').select2()
    </script>


    {{--<script type="text/javascript">--}}
        {{--$(function () {--}}
            {{--$('#datetimepicker1').datetimepicker();--}}
        {{--});--}}
    {{--</script>--}}



@endsection
