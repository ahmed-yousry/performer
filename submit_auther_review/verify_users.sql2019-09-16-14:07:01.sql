-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 14, 2019 at 11:38 AM
-- Server version: 5.7.27-0ubuntu0.18.04.1
-- PHP Version: 7.2.19-0ubuntu0.18.04.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `performa`
--

-- --------------------------------------------------------

--
-- Table structure for table `verify_users`
--

CREATE TABLE `verify_users` (
  `user_id` int(11) NOT NULL,
  `id` int(255) NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `verify_users`
--

INSERT INTO `verify_users` (`user_id`, `id`, `token`, `created_at`, `updated_at`) VALUES
(28, 15, '2zJrpjeTUvpvmXchOZB5GM292za0TQ43mieeJyK4', '2019-09-05 07:15:30', '2019-09-05 07:15:30'),
(29, 16, 'p5sDzpcR1Pu8SMi5lykGyCSoS12ekX9ZSLCKGAEA', '2019-09-05 07:17:23', '2019-09-05 07:17:23'),
(30, 17, 'RbUEf4f9TJGtJUz6bra8WakIhqbs1WwL6WAVmYZX', '2019-09-05 07:20:52', '2019-09-05 07:20:52'),
(31, 18, '24l87BfOsPkrls9TgxWHAsdDcN5d3dFSSydlIOBI', '2019-09-05 07:25:48', '2019-09-05 07:25:48'),
(32, 19, '6dYpFw31OGkiYlxspRmOEhZXT5OpBl4GA8d2bGlv', '2019-09-05 07:33:15', '2019-09-05 07:33:15'),
(24, 20, '6dYpFw31OGkiYlxspRmOEhZXT5OpBl4GA8d2bGlv', '2019-09-05 07:33:15', '2019-09-05 07:33:15'),
(33, 21, '6dYpFw31OGkiYlxspRmOEhZXT5OpBl4GA8d2bGlv', '2019-09-05 07:33:15', '2019-09-05 07:33:15');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `verify_users`
--
ALTER TABLE `verify_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `verify_users`
--
ALTER TABLE `verify_users`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
