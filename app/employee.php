<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class employee extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
     
     protected $fillable = [
     
'fname',
'phone',
'email',
'address',

'deleted_at',
'created_at'


];

    protected $table = 'employees';

    public function attendances(){
        return $this->hasMany('App\AttendanceEmployee');
    }

//     public function education_year(){

// 	    return $this->belongsTo(education_year::class, 'education_year_id');

// }

}

