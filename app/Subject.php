<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    //
    protected $table='subjects';
    protected $fillable = ['name'];

    public function classes(){
        return $this->belongsToMany('App\ClassRoom','subjects_classes','subject_id','class_id')->withTimestamps();
    }

    public function teachers(){
        return $this->belongsToMany('App\teacher','teacher_subject','subject_id','teacher_id')->withTimestamps();
    }
    public function groups(){
        return $this->hasMany('App\Group');
    }
}
