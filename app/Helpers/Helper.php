<?php // Code within app\Helpers\Helper.php

namespace App\Helpers;
use Mail;

class Helper
{
    public static function sendmail($data)
    {


     Mail::send($data['blade-path'], $data, function ($message) use ($data)
        {

              $message->from($data['from']);

            $message->to($data['to']);

            $message->subject($data['subject']);
            
        });


    }



    public static function uploadPaper($request,$destination){

        $filename = $request->getClientOriginalName();
        $mytime = \Carbon\Carbon::now();
        $time = $mytime->toDateTimeString()  ;
        $extension = $request->getClientOriginalExtension();


        $name =  $request->getClientOriginalName().$mytime.'.'.$extension;

        $final_name = str_replace(' ', '-', $name); 

   
         $request->move($destination,$final_name);

        
        return $final_name;


    }


}