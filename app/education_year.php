<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class education_year extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
     
     protected $fillable = [
     
'id',
'name',

];

    protected $table = 'education_years';
}
