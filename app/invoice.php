<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class invoice extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
     
     protected $fillable = [
     
'student_id',
'group_id',
'month',
'value',
'seg',


'deleted_at',
'created_at'


];

    protected $table = 'invoices';
    

    public function student_id(){

	    return $this->belongsTo(student::class, 'student_id');

}

public function group_id(){

    return $this->belongsTo(Group::class, 'group_id');

}




}

