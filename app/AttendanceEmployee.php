<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AttendanceEmployee extends Model
{
    //
    protected $fillable=['employee_id','state'];

    public function employee(){
        return $this->belongsTo('App\employee','employee_id');
    }
}
