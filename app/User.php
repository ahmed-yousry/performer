<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use App\type;
use App\u_roles;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'users';





    protected $fillable = [
        'id','name', 'email', 'password','familyName','Title','Affilation','Country','verified','user_type_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
       'password', 'remember_token','api_token',
    //'password', 'remember_token',
    ];



    public function roles(){

         return $this->belongsToMany('App\u_roles','user_roles','user_id','role_id');

        //return $this->belongsToMany('App\Models\Role','admin_role','admin_id','role_id');


    }



             public function get_user_type_id(){



                return $this->belongsTo(type::class,'user_type_id');


        }


        public function verifyUser()
{
    return $this->hasOne('App\VerifyUser');
}




}
