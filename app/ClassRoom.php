<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClassRoom extends Model
{
    //
    protected $table='classes';
    protected $fillable = ['name','education_year_id'];

    public function education(){
        return $this->belongsTo('App\EducationYear','education_year_id');
    }
    public function subjects(){
        return $this->belongsToMany('App\Subject','subjects_classes','class_id','subject_id')->withTimestamps();
    }
    public function groups(){
        return $this->hasMany('App\Group');
    }
    public function students(){
        return $this->hasMany('App\student');
    }
}
