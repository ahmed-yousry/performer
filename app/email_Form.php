<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\email_Templet;

class email_Form extends Model
{
    //

    protected $fillable = [

'user_templet_id',
'subject',
'body',
 



];


protected $table = 'email__forms';


   public function get_email_Templet(){
       return $this->belongsTo(email_Templet::class, 'user_templet_id');
}


}
